# Recipe App API proxy

Nginx proxy for our recipe app API

## Usage

### Environment varialbles
* `LISTEN_PORT` - Port tp listen on (default: `8000`)
* `APP_HOST`    - Hostname of the app to forward requests (default: `app`)
* `APP_PORT`    - Port of the app to forward requests to (default: `9000`)